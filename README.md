# OpenML dataset: Nutritional-Facts-for-most-common-foods

https://www.openml.org/d/43423

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Everybody nowadays is mindful of what they eat. Counting calories and reducing fat intake is the number one advice given by all dieticians and nutritionists. Therefore, we need to know what foods are rich in what nutrients, don't we?
Content
The dataset contains a csv file with more than 300 foods each with the amount of Calories, Fats, Proteins, Saturated Fats, Carbohydrates, Fibers labelled for each food. Also, the foods are also categorised into various groups like Desserts, Vegetables, Fruits etc.
Note: "t" indicates that only a trace amount is available(miniscule) 
Acknowledgements
References: 

Food Nutrient List from Wikipedia

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43423) of an [OpenML dataset](https://www.openml.org/d/43423). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43423/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43423/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43423/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

